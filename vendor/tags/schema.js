"use strict";

const fs = require('fs');
const utils = require('../utils');
const settings_data = JSON.parse(fs.readFileSync('theme/config/settings_data.json'));

Object.defineProperty(exports, "__esModule", { value: true });
exports.Schema = void 0;
function generateSettingsObj(settings) {
    if (!Array.isArray(settings)) {
        return settings;
    }
    return settings
        .filter((entry) => !!entry.id)
        .reduce((sectionSettings, entry) => {
        sectionSettings[entry.id] = entry.default;
        return sectionSettings;
    }, {});
}
function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
}
function mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, { [key]: source[key] });
            }
        }
    }

    return mergeDeep(target, ...sources);
}
exports.Schema = {
    parse: function (tagToken, remainTokens) {
        this.tokens = [];
        const stream = this.liquid.parser.parseStream(remainTokens);
        stream
            .on('token', (token) => {
            if (token.name === 'endschema') {
                stream.stop();
            }
            else
                this.tokens.push(token);
        })
            .on('end', () => {
            throw new Error(`tag ${tagToken.getText()} not closed`);
        });
        stream.start();
    },
    render: function (ctx) {

        const json = this.tokens.map((token) => token.getText()).join('');
        const schema = JSON.parse(json);
        const scope = ctx.scopes[ctx.scopes.length - 1];

        let defaultSettings = generateSettingsObj(schema.settings);
        let section = utils.string_to_slug(schema.name);
        let settings = defaultSettings;

        if( 'current' in settings_data && 'sections' in settings_data.current && section in settings_data.current.sections ){

            let currentSettings = settings_data.current.sections[utils.string_to_slug(schema.name)].settings;
            settings = mergeDeep(defaultSettings, currentSettings);
        }

        scope.section = {
            settings: settings,
            blocks: (schema.blocks || []).map((block) => ({
                ...block,
                settings: generateSettingsObj(block.settings)
            }))
        };
        return '';
    }
};
