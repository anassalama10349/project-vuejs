"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Form = void 0;
exports.Form = {
    parse: function (tagToken, remainTokens) {
        this.tokens = [];
        const stream = this.liquid.parser.parseStream(remainTokens);
        stream
            .on('token', (token) => {
            if (token.name === 'endform')
                stream.stop();
            else
                this.tokens.push(token);
        })
            .on('end', () => {
            throw new Error(`tag ${tagToken.getText()} not closed`);
        });
        stream.start();
    },
    render: function* (ctx, emitter) {
        const template = this.tokens.map((token) => token.getText()).join('');
        return `<form>${this.liquid.parseAndRenderSync(template, ctx.environments, emitter)}</form>`;
    }
};
