"use strict";

const fs = require('fs');
const settings_data = JSON.parse(fs.readFileSync('theme/config/settings_data.json'));

Object.defineProperty(exports, "__esModule", { value: true });
exports.Section = void 0;
const quoted = /^'[^']*'|"[^"]*"$/;
exports.Section = {
    parse: function (token) {
        this.namestr = token.args;
    },
    render: function* (ctx, emitter) {
        let name;
        let template_name = this.namestr.slice(1, -1);
        if (quoted.exec(this.namestr)) {
            const template = '../sections/'+template_name+'.liquid';
            name = yield this.liquid._parseAndRender(template, ctx.getAll(), ctx.opts);
        }
        if (!name)
            throw new Error('cannot include with empty filename');

        const templates = yield this.liquid._parseFile(name, ctx.opts, ctx.sync);

        // Bubble up schema tag for allowing it's data available to the section
        templates.sort((tagA) => {
            return tagA.token.kind === 4 &&
                tagA.token.name === 'schema'
                ? -1
                : 0;
        });

        if( 'current' in settings_data && 'sections' in settings_data.current && template_name in settings_data.current.sections )
            ctx.scopes[ctx.scopes.length - 1][template_name] = settings_data.current.sections[template_name]

        yield this.liquid.renderer.renderTemplates(templates, ctx, emitter);
    }
};
