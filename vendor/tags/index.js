"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

exports.liquidTags = void 0;

const javascript = require("./javascript");
const schema = require("./schema");
const section = require("./section");
const stylesheet = require("./stylesheet");
const style = require("./style");
const form = require("./form");
const paginate = require("./paginate");

function liquidTags() {
    return function () {
        this.registerTag('section', section.Section);
        this.registerTag('schema', schema.Schema);
        this.registerTag('stylesheet', stylesheet.StyleSheet);
        this.registerTag('style', style.Style);
        this.registerTag('javascript', javascript.JavaScript);
        this.registerTag('form', form.Form);
        this.registerTag('paginate', paginate.Paginate);
    };
}
exports.liquidTags = liquidTags;
