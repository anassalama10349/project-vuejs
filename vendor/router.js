const express = require('express');
const router = express.Router();
const fs = require('fs');

const globals = JSON.parse(fs.readFileSync('data/globals.json'));

// global render function
const render = function (res, req, title, template, options){

    options.settings = globals.settings;
    options.page_title = title;

    if( 'view' in req.query )
        template += '.'+req.query.view

    options.template = template;

    res.render('./templates/'+template+'.liquid', options);
}


// home page route
router.get('/', function(req, res) {

    const index = JSON.parse(fs.readFileSync('data/index.json'));

    render(res, req, 'Homepage', 'index', { layout: index})
});


// collections route
router.get('/collections', function(req, res) {

    const collections = JSON.parse(fs.readFileSync('data/collections.json'));

    // remove first item which is /all
    collections.shift();

    collections.forEach(function (collection) {
        collection.url = "/collections/"+collection.handle;
    });

    render(res, req, 'Collections', 'list-collections', { collections: collections })
});


// collection route
router.get('/collections/:id', function(req, res) {

    let collection = false;

    const collections = JSON.parse(fs.readFileSync('data/collections.json'));
    collections.forEach(function (item) {
        if( item.handle === req.params.id)
            collection = item;
    });

    if( !collection )
        return res.render('./templates/404');

    collection.url = "/collections/"+collection.handle;
    const products = JSON.parse(fs.readFileSync('data/products.json'));

    products.forEach(function (element) {
        element.url = "/products/"+element.handle;
        element.has_only_default_variant = element.variants[0].title === 'Default title';
    });

    collection.products = products;

    render(res, req, collection.title, 'collection', { collection: collection, current_page:1 })
});


// product route
router.get('/products/:id', function(req, res) {

    const products = JSON.parse(fs.readFileSync('data/products.json'));
    let product = false;

    products.forEach(function (element) {
        if( element.handle === req.params.id )
            product = element;
    });

    if( !product )
        return res.render('./templates/404');

    product.url = '/products/'+product.handle
    product.has_only_default_variant = product.variants[0].title === 'Default title';

    render(res, req, product.title, 'product', { product: product })
});


// article route
router.get('/blogs/news/:id', function(req, res) {

    const blog = JSON.parse(fs.readFileSync('data/blog.json'));
    let article = false;

    blog.articles.forEach(function (element) {
        if( element.handle === req.params.id )
            article = element;
    });

    if( article )
        article.url = '/blogs/news/'+article.handle

    render(res, req, article.title, 'article', { article: article })
});


// article route
router.get('/blogs/news', function(req, res) {

    const blog = JSON.parse(fs.readFileSync('data/blog.json'));

    blog.articles.forEach(function (article) {
        article.url = '/blogs/news/'+article.handle
    });

    render(res, req, blog.title, 'blog', { blog: blog })
});


// article route
router.get('/pages/:id', function(req, res) {

    const pages = JSON.parse(fs.readFileSync('data/pages.json'));
    let page = false;
    let template = false;

    pages.forEach(function (element) {
        if( element.handle === req.params.id )
            page = element;
    });

    if( page )
        template = 'template' in page ? 'page.'+page.template : 'page';

    render(res, req, page.title, template, { page: page })
});


// account
router.get('/account', function(req, res) {

    render(res, req, 'Account', 'customers/account', { customer: globals.customer })
});


// account login
router.get('/account/login', function(req, res) {

    if( 'customer' in req.query )
        res.redirect('/account');
    else
        render(res, req, 'Account', 'customers/login', { form:{ password_needed: true } })
});


// account addresses
router.get('/account/addresses', function(req, res) {

    render(res, req, 'Account', 'customers/addresses', { customer: globals.customer })
});


// account reset
router.get('/account/reset', function(req, res) {

    render(res, req, 'Reset account', 'customers/reset_password', {})
});


// account activate
router.get('/account/activate', function(req, res) {

    render(res, req, 'Activate account', 'customers/activate_account', {})
});


// account register
router.get('/account/register', function(req, res) {

    render(res, req, 'Register', 'customers/register', {})
});


// account orders
router.get('/account/orders/:id', function(req, res) {

    let order = globals.customer.orders[0];
    order.billing_address = globals.customer.addresses[0];
    order.shipping_address = globals.customer.addresses[1];

    render(res, req, 'Order', 'customers/order', {customer: globals.customer, order: order})
});


// cart
router.get('/cart', function(req, res) {

    render(res, req, 'Cart', 'cart', {customer: globals.customer})
});


// search
router.get('/search', function(req, res) {

    let search = {};
    if( 'q' in req.query ){

        const pages = JSON.parse(fs.readFileSync('data/pages.json'));
        const blog = JSON.parse(fs.readFileSync('data/blog.json'));
        const products = JSON.parse(fs.readFileSync('data/products.json'));

        search = {
            performed: true ,
            results_count: 4,
            terms: req.query.q,
            results:[
                pages[0], blog.articles[0], products[0], products[1]
            ]
        }
    }

    render(res, req, 'Search results', 'search', {search:search})
});

// notifications route for emails
router.get('/notifications/:id', function(req, res) {

    const notifications = JSON.parse(fs.readFileSync('data/notifications.json'));

    let options = req.params.id in notifications ? notifications[req.params.id] : {};
    options.shop = globals.shop;
    options.customer = globals.customer;

    options = {...options, ...notifications.order}

    res.render('./notifications/'+req.params.id+'.liquid', options);
});

module.exports = router;
