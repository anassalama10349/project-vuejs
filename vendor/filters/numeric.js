
module.exports.atMost = function atMost(value, max) {

    if( typeof value != "undefined" )
        return Math.min(value, max);
    else
        return max;
};
