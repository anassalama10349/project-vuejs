"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

exports.liquidFilters= void 0;

const font_face = require("./font_face");
const font_modify = require("./font_modify");
const translate = require("./translate");
const links = require("./links");
const numeric = require("./numeric");
const string = require("./string");
const money = require("./money");
const tag = require("./tag");
const default_pagination = require("./default_pagination");
const address = require("./address");

function liquidFilters() {
    return function () {
        this.registerFilter('font_face', font_face.fontFace);
        this.registerFilter('font_modify', font_modify.fontModify);
        this.registerFilter('t', translate.translate);
        this.registerFilter('customer_login_link', links.CustomerLoginLink);
        this.registerFilter('customer_register_link', links.CustomerRegisterLink);
        this.registerFilter('customer_logout_link', links.CustomerLogoutLink);
        this.registerFilter('link_to_tag', links.linkToTag);
        this.registerFilter('at_most', numeric.atMost);
        this.registerFilter('link_to', links.linkTo);
        this.registerFilter('format_address', address.formatAddress);
        this.registerFilter('img_url', links.imgUrl);
        this.registerFilter('asset_url', links.assetUrl);
        this.registerFilter('payment_icon_png_url', links.paymentIconPngUrl);
        this.registerFilter('shopify_asset_url', links.shopifyAssetUrl);
        this.registerFilter('money', money.money);
        this.registerFilter('money_with_currency', money.moneyWithCurrency);
        this.registerFilter('stylesheet_tag', tag.stylesheet);
        this.registerFilter('script_tag', tag.script);
        this.registerFilter('img_tag', tag.img);
        this.registerFilter('time_tag', translate.timeTag);
        this.registerFilter('default_pagination', default_pagination.defaultPagination);
        this.registerFilter('handleize', string.handleize);
        this.registerFilter('date', string.date);
        this.registerFilter('color_lighten', string.colorLighten);
    };
}
exports.liquidFilters = liquidFilters;
