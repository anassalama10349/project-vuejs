module.exports.fontFace = function fontFace(font) {

    let s = font.style.substring(0,1);
    let w = font.weight.toString().substring(0,1);

    if( w === 'b' )
        w = 7;

    if( s === 'i' )
        s = 'o';

    let variant = s+w;

    return '@font-face {\n' +
        '  font-family: '+font.family+';\n' +
        '  font-weight: '+font.weight+';\n' +
        '  font-style: '+font.style+';\n' +
        '  src: url("'+font[variant].woff2+'") format("woff2"),\n' +
        '       url("'+font[variant].woff+'") format("woff");\n' +
        '}';
};
