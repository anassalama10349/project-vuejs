module.exports.formatAddress = function formatAddress(s) {

    return s ? '<p>'+
        s.first_name + ' ' + s.last_name + '<br>' +
        s.company + '<br>' +
        s.address1 + '<br>' +
        s.city + ' ' + s.zip + '<br>' +
        s.country +
        '</p>' : ''
};