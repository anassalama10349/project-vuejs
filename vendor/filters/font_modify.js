module.exports.fontModify = function fontModify(font, key, value) {
    let _font = JSON.parse(JSON.stringify(font));
    _font[key]= value;
    return _font;
};
