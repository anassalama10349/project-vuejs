module.exports.money = function money(price) {
    if (typeof price == "undefined") {
        return '';
    }

    if (!price.currencyCode || !price.amount) {

        return new Intl.NumberFormat('en', {
            style: 'currency',
            currency: 'EUR',
            maximumFractionDigits: 2,
        }).format(price/100);
    }
    // the price that this object gets has 2 fields it is not the same value in "real" env,
    // at real it should be only a number multiplied by 100
    return new Intl.NumberFormat('en', {
        style: 'currency',
        currency: price.currencyCode,
        maximumFractionDigits: 2,
    }).format(price.amount);
};

module.exports.moneyWithCurrency = function moneyWithCurrency(price) {

    return module.exports.money(price)+' EUR'
};
