const fs = require('fs');
const trads = JSON.parse(fs.readFileSync('theme/locales/en.default.json'));
const strftime = require('strftime');

module.exports.translate = function translate(key, args, args2) {

    function index(obj,i) {return obj[i]}
    let trad = key.split('.').reduce(index, trads);

    if( args ){

        if( args[0] === 'count' ){

            if( args[1] < 2 )
                trad = trad.one
            else
                trad = trad.other
        }

        if( args2 && args2[0] === 'count' ){

            if( args2[1] < 2 )
                trad = trad.one
            else
                trad = trad.other
        }

        if (trad){

            for (let i = 1; i < arguments.length; i++) {
                trad = trad.replace('{{ '+arguments[i][0]+' }}', arguments[i][1]);
            }
        }
    }

    return trad?trad:key
};

module.exports.timeTag = function timeTag(date, args) {

    function index(obj,i) {return obj[i]}

    let format = ('date_formats.'+args[1]).split('.').reduce(index, trads);
    date = new Date( Date.parse(date) );

    return strftime(format, date);
};
