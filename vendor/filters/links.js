const utils = require('../utils');

module.exports.CustomerLoginLink = function CustomerLoginLink(s) {

    return '<a href="/account/login">'+s+'</a>'
};

module.exports.CustomerLogoutLink = function CustomerLogoutLink(s) {

    return '<a href="/account/logout">'+s+'</a>'
};

module.exports.CustomerRegisterLink = function CustomerRegisterLink(s) {

    return '<a href="/account/register">'+s+'</a>'
};

module.exports.linkToTag = function linkToTag(s, tag) {

    return '<a href="/collections/'+utils.string_to_slug(tag)+'">'+s+'</a>'
};

module.exports.linkTo = function linkTo(s, url) {

    return '<a href="'+url+'">'+s+'</a>'
};

module.exports.assetUrl = function assetUrl(s) {

    return '/'+s
};

module.exports.paymentIconPngUrl = function paymentIconPngUrl(s) {

    return '/notifications/'+s.toLowerCase().replace(/ /g, "_")+'.png'
};

module.exports.shopifyAssetUrl = function shopifyAssetUrl(s) {

    return '/'+s
};

module.exports.imgUrl = function imgUrl(s, size) {

    if( typeof s == 'object' && 'featured_image' in s )
        s = s.featured_image;

    if( typeof s == 'object' && 'image' in s )
        s = s.image;

    if( typeof s == 'object' && 'src' in s )
        s = s.src;

    return s ? s.replace('.com', '.com/'+size).replace('.png', '_'+size+'.png').replace('.jpg', '_'+size+'.jpg') : ''
};

module.exports.linkToRemoveTag = function linkToRemoveTag(s, tag) {

    return '<a>'+s+'</a>'
};
