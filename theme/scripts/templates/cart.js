import Vue from "vue";

Vue.component('cart', {
  methods:{
    closeCart(){
      this.$emit('close-cart');
    }
  },
  computed:{
    cart(){
      return this.$store.getters.cart()
    }
  }
});

Vue.component('cart-line', {
  props:['line','item'],
  methods:{
    featured_image(image){
      return image.replace(/(\.[^.]*)$/, "_100x100_crop_center$1").replace('http:', '')
    },
    remove(){
      this.$http.post('/cart/change.js',{'quantity': 0, 'line': this.line }).then(response => {
        this.$store.commit('cart',response.body)
      }, response=>{
        console.log('erreur' + response)
      });
    },
    increment(type){
      let quantity = this.item.quantity;
      if( type ==='up')
        quantity++
      else
        quantity--

      this.$http.post('/cart/change.js',{'quantity': quantity, 'line': this.line }).then(response => {
        this.$store.commit('cart',response.body)
      }, response=>{
        console.log('erreur' + response)
      });
    }


  }

});