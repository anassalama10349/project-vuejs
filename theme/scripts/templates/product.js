import {load} from '@shopify/theme-sections';
import '../sections/product';

load('*');

import Vue from "vue";

Vue.component('tabs', {
	template: `
        <div>
            <div class="tabs">
              <ul>
                <li v-for="tab in tabs" :class="{ 'is-active': tab.isActive }">
                    <a :href="tab.href" @click="selectTab(tab)">{{ tab.name }}</a>
                </li>
              </ul>
            </div>

            <div class="tabs-details">
                <slot></slot>
            </div>
        </div>
    `,

	data() {
		return {tabs: [] };
	},

	created() {

		this.tabs = this.$children;

	},
	methods: {
		selectTab(selectedTab) {
			this.tabs.forEach(tab => {
				tab.isActive = (tab.name == selectedTab.name);
			});
		}
	}
});

Vue.component('tab', {

	template: `

        <div v-show="isActive"><slot></slot></div>

    `,

	props: {
		name: { required: true },
		selected: { default: false}
	},

	data() {

		return {
			isActive: false
		};

	},

	computed: {

		href() {
			return '#' + this.name.toLowerCase().replace(/ /g, '-');
		}
	},

	mounted() {

		this.isActive = this.selected;

	}
});

Vue.component('product', {
	data(){
		return{
			sticky_top: false,
			sticky_infos: false,
			sticky_bottom_infos: false,
			lastScrollTop: 0,
			fromTop: 0,
			simulateTouch:false,
			productOptions: {
				spaceBetween: 10,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev'
				}
			},
			productOptionsThumbs: {
				direction: 'vertical',
				spaceBetween: 10,
				slidesPerView: 5,
				slideToClickedSlide: true
			}
		}
	},
	mounted() {
		const swiperTop = this.$refs.swiperProduct.swiper
		const swiperThumbs = this.$refs.swiperProductThumbs.swiper
		this.$nextTick(() => {
			swiperTop.controller.control = swiperThumbs
			swiperThumbs.controller.control = swiperTop
		})
	}
});
