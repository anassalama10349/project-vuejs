import Vue from "vue";

Vue.component('language-switcher', {
	data(){
		return{
			input_value: false
		}
	},
	props:['locales','current'],
	delimiters: ['[[', ']]'],
	template: '<ul>' +
				'<li class="language-switcher__item" v-for="(locale,index) in JSON.parse(locales)" :key="index">' +
				'<div @click="switchLanguage(locale.shop_locale.locale)">[[ locale.shop_locale.locale ]]</div>' +
				'</li>' +
			'</ul>',
	methods:{
		switchLanguage(code){

				if(this.code !== this.input_value){
					this.input_value = code;
					document.getElementById('locale_code').value = code
					document.getElementById('localization_form').submit()
				}
		}
	},
	mounted(){
		this.input_value = JSON.parse(this.current).shop_locale.locale
	}
});

Vue.component('banner', {
	data(){
		return{
			show_banner: false
		}
	},
	methods:{
		hideBanner(){
				this.show_banner = false
				window.sessionStorage.setItem('bannerDismissed', true)
		}
	},
	mounted(){
		this.show_banner = !window.sessionStorage.getItem('bannerDismissed')
	}
});

Vue.component('burger', {
	template: '<div class="a-burger">' +
			'<a class="a-burger__link" v-on:click="toggleVisibility"></a>' +
			'<span class="a-burger__icon"></span>' +
			'</div>',
	methods:{
		toggleVisibility: function(){
			document.body.classList.toggle('burger-is-open')
		}
	}
});

Vue.component('nav-component', {
	data(){
		return{
			show_submenu: false
		}
	}
});
