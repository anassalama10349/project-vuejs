import Vue from "vue";

Vue.directive("toggle-active", {
	bind: function(el, binding, vnode) {
		el.addEventListener(
				"click",
				() => {
					let active = vnode.context.class_active;
					active = !active;
					vnode.context.class_active = active;

					let element = el;
					if(binding.modifiers.parent)
						element = el.parentElement
					if (!active) {
						element.classList.remove("active");
						element.classList.add("not-active");
					} else {
						element.classList.remove("not-active");
						element.classList.add("active");
					}
				},
				false
		);
	}
});