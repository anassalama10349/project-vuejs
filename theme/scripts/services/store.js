import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

let store = new Vuex.Store({
  state(){
    return {
      "cart": {
        "token": "1659a155e0110286f9ab960e00405f24",
        "note": null,
        "attributes": {},
        "original_total_price": 29500,
        "total_price": 29500,
        "total_discount": 0,
        "total_weight": 0,
        "item_count": 2,
        "items": [
          {
            "id": 40085773385889,
            "properties": {},
            "quantity": 1,
            "variant_id": 40085773385889,
            "key": "40085773385889:174d3b3bae9d2f5981af43a0e5073e89",
            "title": "Tabac noir",
            "price": 29500,
            "original_price": 29500,
            "discounted_price": 29500,
            "line_price": 29500,
            "original_line_price": 29500,
            "total_discount": 0,
            "discounts": [],
            "sku": "",
            "grams": 0,
            "taxable": true,
            "product_id": 6772477001889,
            "product_has_only_default_variant": true,
            "gift_card": false,
            "final_price": 29500,
            "final_line_price": 29500,
            "url": "/products/activ5?variant=40085773385889",
            "featured_image": {
              "aspect_ratio": 0.667,
              "alt": "Alex",
              "height": 2560,
              "url": "https://cdn.shopify.com/s/files/1/0564/4940/0993/products/97be261648f234d76853f3f2e3858500.jpg?v=1623750686",
              "width": 1707
            },
            "image": "https://cdn.shopify.com/s/files/1/0564/4940/0993/products/97be261648f234d76853f3f2e3858500.jpg?v=1623750686",
            "handle": "alex",
            "requires_shipping": true,
            "product_type": "",
            "product_title": "Tabac noir",
            "product_description": "Appareil d'entraînement connecté\nConçu pour favoriser la pratique régulière d'une activité physique, Activ5 est un appareil de petite taille facile à transporter et facile à utiliser. Développé après 5 années de recherche et développement, ce galet est doté de capteurs de force permettant de mesurer la contraction musculaire statique de son utilisateur (principes d'isométrie). Grâce à l'application mobile qui accompagne Activ5 (iOS et Android), vous pouvez découvrir de nouveaux exercices, calibrer leur difficulté, suivre vos progrès et adapter vos entraînements. Avec Activ5, il n'a jamais été aussi simple de rester en forme.\n",
            "variant_title": null,
            "variant_options": [
              "Default Title"
            ],
            "options_with_values": [
              {
                "name": "type",
                "value": "Eau de parfum"
              },
              {
                "name": "flacon",
                "value": "Vaporisateur 50ml"
              }
            ],
            "line_level_discount_allocations": [],
            "line_level_total_discount": 0
          },
          {
            "id": 40085773385889,
            "properties": {},
            "quantity": 1,
            "variant_id": 40085773385889,
            "key": "40085773385889:174d3b3bae9d2f5981af43a0e5073e89",
            "title": "Tabac noir",
            "price": 29500,
            "original_price": 29500,
            "discounted_price": 29500,
            "line_price": 29500,
            "original_line_price": 29500,
            "total_discount": 0,
            "discounts": [],
            "sku": "",
            "grams": 0,
            "taxable": true,
            "product_id": 6772477001889,
            "product_has_only_default_variant": true,
            "gift_card": false,
            "final_price": 29500,
            "final_line_price": 29500,
            "url": "/products/activ5?variant=40085773385889",
            "featured_image": {
              "aspect_ratio": 0.667,
              "alt": "Alex",
              "height": 2560,
              "url": "https://cdn.shopify.com/s/files/1/0564/4940/0993/products/97be261648f234d76853f3f2e3858500.jpg?v=1623750686",
              "width": 1707
            },
            "image": "https://cdn.shopify.com/s/files/1/0564/4940/0993/products/97be261648f234d76853f3f2e3858500.jpg?v=1623750686",
            "handle": "alex",
            "requires_shipping": true,
            "product_type": "",
            "product_title": "Tabac noir",
            "product_description": "Appareil d'entraînement connecté\nConçu pour favoriser la pratique régulière d'une activité physique, Activ5 est un appareil de petite taille facile à transporter et facile à utiliser. Développé après 5 années de recherche et développement, ce galet est doté de capteurs de force permettant de mesurer la contraction musculaire statique de son utilisateur (principes d'isométrie). Grâce à l'application mobile qui accompagne Activ5 (iOS et Android), vous pouvez découvrir de nouveaux exercices, calibrer leur difficulté, suivre vos progrès et adapter vos entraînements. Avec Activ5, il n'a jamais été aussi simple de rester en forme.\n",
            "variant_title": null,
            "variant_options": [
              "Default Title"
            ],
            "options_with_values": [
              {
                "name": "type",
                "value": "Eau de parfum"
              },
              {
                "name": "flacon",
                "value": "Vaporisateur 50ml"
              }
            ],
            "line_level_discount_allocations": [],
            "line_level_total_discount": 0
          }
        ],
        "requires_shipping": true,
        "currency": "EUR",
        "items_subtotal_price": 29500,
        "cart_level_discount_applications": []
      }
    }
  },
  getters: {
    cart: state => () =>{
      return state.cart;
    }
  },
  mutations:{
    cart(state,cart){
      state.cart = cart;
    }
  },
  actions: {
    initCart(context){
      // Vue.http.get('/cart.js').then(response => {
      //   context.commit('cart', response.body);
      // }, response=>{
      //   console.log('erreur' + response)
      // });
    }
  }
});

export default store;
