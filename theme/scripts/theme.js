// Import scss
import '../styles/theme.scss'

import './templates/customers/login'
import './templates/customers/addresses'


// load Vuejs
import Vue from "vue";

import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper);

import store from './services/store'
import './services/directives'

Vue.directive('toggle-active')

// load design system atoms, molecules, organisms and pages
let components = require.context("./templates", true, /^\.\/.*\.js/);
components.keys().forEach(components);

// start app
let bundle = new Vue({
	store,
	el: '#app',
	data(){
		return {
			isMobile: false,
			sticky: false,
			showSticky: true,
			drawerLogin: false,
			drawerCart: false,
			accountAddress: false,
			pushProducts:{
				slidesPerView: 3,
				watchOverflow: true,
				pagination: {
					el: '.sn-push-products__pagination',
					type: 'progressbar'
				},
				navigation: {
					nextEl: '.sn-push-products__next',
					prevEl: '.sn-push-products__prev'
				},
				breakpoints: {
					991: {
					  slidesPerView: 2,
					  spaceBetween: 0
					},
					768: {
					slidesPerView: 1.50,
					spaceBetween: 0
					}
				},
				swiperOptions: {
					pagination: {
						el: ".swiper-pagination",
						clickable: true
					},
				}
			},
			exepProducts:{
				slidesPerView: 3,
				breakpoints: {
					991: {
					  slidesPerView: 2,
					  spaceBetween: 0
					},
					768: {
						slidesPerView: 1,
						spaceBetween: 0
					}
				},
			}
		}
	},
	delimiters: ['[[', ']]'],
	methods:{
		editAddress(id){
			var element = document.querySelector('.t-account__addresses-saved .t-account__form');
			var classList = element.classList;
			if(classList.contains('t-account__form--hidden')){
				element.id = 'EditAddress_' + id
				classList.remove('t-account__form--hidden')
			}
			else{
				element.id = 'EditAddress_'
				classList.add('t-account__form--hidden')
			}
		},
		editFav(id){
			let element = document.querySelector('#EditAddress_'+id).classList;

			if(element.contains('t-account__form--hidden'))
				element.remove('t-account__form--hidden')
			else
				element.add('t-account__form--hidden')

		},
		deleteAddress(message){
			return window.confirm(message);
		},
		cartOpen(){
			this.drawerCart = true
		},
		formatPrice(value){
			return Shopify.formatMoney(value,Shopify.money_format)
		},
		catchScroll:function() {
			this.sticky = window.pageYOffset > 140 && this.showSticky;
			if(this.sticky){
				let element = document.querySelector('.s-header__main')
				// if(element)
				// 	let headerHeight = element.clientHeight;
			}
		},
		catchResize: function(event){
			this.isMobile = window.innerWidth < 769;
		},
		stickyProduct(value){
			this.showSticky = value
		}
	},
	computed:{
		cart(){
			return this.$store.getters.cart()
		}
	},
	mounted: function(){

		let body_classList = document.body.classList;
		body_classList.remove('loading');
		body_classList.add('loaded');

		this.sticky = window.pageYOffset > 140 && !this.isProductSticky;
		this.isMobile = window.innerWidth < 769;

		const headerHeight = document.getElementById("header").offsetHeight;
		const navHeight = document.getElementById("nav").offsetHeight;
		const homeBody = document.querySelector("#Homepage main");
		const height = headerHeight + navHeight;


		if(document.body.contains(homeBody) == true){
			homeBody.style.marginTop = - height + "px";
		}
		window.addEventListener('resize', function(event) {
			const headerHeight = document.getElementById("header").offsetHeight;
			const navHeight = document.getElementById("nav").offsetHeight;
			const height = headerHeight + navHeight;
			if(document.body.contains(homeBody) == true){
				homeBody.style.marginTop = - height + "px";
			}
		}, true);


		const burger1 = document.getElementById('burger1');
		const burger2 = document.getElementById('burger2');
		const nav = document.getElementById("menu-mobile");
		burger1.addEventListener("click", function() {
			nav.classList.add("open");
		});
		burger2.addEventListener("click", function() {
			nav.classList.remove("open");
		})

		const checkBox = document.querySelectorAll("input[type='checkbox']");

		checkBox.forEach(function(element){
			element.addEventListener("click", function(){
				if (element.checked == true){
					element.classList.add("checked");
				} else {
					element.classList.remove("checked");
				}
			});
		})

		const childLink = document.querySelectorAll("nav ul li");
		childLink.forEach(element => {
			element.addEventListener("mouseover", function(){
				element.classList.add("active");
			})
			element.addEventListener("mouseout", function(){
				element.classList.remove("active");
			})
		})

		const lang = document.querySelector(".menu__mobile__info .language-switcher__label");
		if(document.body.contains(lang)){
			lang.addEventListener("click", function(){
				this.nextElementSibling.classList.toggle("open");
			})
		}

		const ul = document.querySelectorAll(".menu__mobile__nav ul");
		ul.forEach(function(element) {

			const lis = element.childNodes;

			lis.forEach(function(li) {
				li.addEventListener("click", function(e){
					if(this.classList.contains("active") == false){
						const children = this.parentNode.children;
						for(let i = 0; i < children.length; i++){
							children[i].classList.remove("active");
						}
						this.classList.add("active");
					}
				})
			})
		})

		const return1 = document.getElementById("return1");
		const return2 = document.getElementById("return2");
		return1.addEventListener("click", function(e){
			e.stopPropagation()
			this.closest("li").classList.remove("active");
		})
		return2.addEventListener("click", function(e){
			e.stopPropagation()
			this.closest("li").classList.remove("active");
		})


		const radioSwitch = document.querySelectorAll("input[name='flacons']");
		const radioSelect = document.querySelector(".toggle-radio .switch");
		radioSwitch.forEach(function(element){
			element.addEventListener('change', function() {
				if(this.checked == true){
					var value = this.value;
					if(value == "flacons-o"){
						radioSelect.classList.remove("flacons-picots");
						radioSelect.classList.add("flacons-o");
					}
					else if(value == "flacons-picots"){
						radioSelect.classList.remove("flacons-o");
						radioSelect.classList.add("flacons-picots");
					}
				}
			});
		});
		if(document.body.contains(radioSelect)){
			radioSelect.addEventListener('click', function() {
				if(this.classList.contains("flacons-o") == true){
					this.classList.remove("flacons-o");
					this.classList.add("flacons-picots");
					document.getElementById("flacons-o").checked = false;
					document.getElementById("flacons-picots").checked = true;
				}
				else if(this.classList.contains("flacons-picots") == true){
					this.classList.remove("flacons-picots");
					this.classList.add("flacons-o");
					document.getElementById("flacons-o").checked = true;
					document.getElementById("flacons-picots").checked = false;
				}
			})
		}

		const categorie = document.querySelectorAll(".sn-faq-sidebar__category");
		categorie.forEach(function(element){
			element.addEventListener("click", function(e){
				if(this.classList.contains("sn-faq-sidebar__category--")){
					e.preventDefault();
					for (var i = 0, len = categorie.length; i < len; i++) {
						categorie[i].classList.remove("sn-faq-sidebar__category--active");
						categorie[i].classList.add("sn-faq-sidebar__category--");
					}
					this.classList.remove("sn-faq-sidebar__category--");
					this.classList.add("sn-faq-sidebar__category--active");
				}
			})
		})

		const focus = document.querySelectorAll('.form-input input');

		focus.forEach(function(element){

			element.addEventListener('focus', (e) => {
				element.classList.add("focused");
			});
			element.addEventListener('blur', (e) => {
				if(!element.value.trim().length != 0){
					element.classList.remove("focused");
				}
			});
		})

		let popup = document.getElementById('popin');
		let overlay = document.querySelector("#popin .overlay");
		let btnClose = document.querySelector("#popin .close");
		if(document.body.contains(popup)){
			setTimeout(function(){ 
				popup.classList.add("open");
			}, 3000);
			overlay.addEventListener("click", function(e){
				if(e.target == e.currentTarget){
					popup.classList.remove("open");
				}
			});
			btnClose.addEventListener("click", function(){
				popup.classList.remove("open");
			});
		}
	},
	created() {
		window.addEventListener('scroll', this.catchScroll);
		window.addEventListener('resize', this.catchResize);
	},
	destroyed() {
		window.removeEventListener('scroll', this.catchScroll);
		window.removeEventListener('resize', this.catchResize);
	}
});