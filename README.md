# 📦 Shopify Theme Toolkit

This project allows developing theme for Shopify locally without any connection required.

Local webserver, hot reload, webpack compilation.

## Installation

[Download repository](https://github.com/wearemetabolism/shopify-theme-toolkit/archive/refs/heads/develop.zip)

Go to the folder then run

``npm install``

## Folders

- data : static data for products, pages, collection, etc ...
- theme : Shopify naked theme
- vendor : Shopify specific filters and tags

## Node

This project has been tested on node 14.17 and npm 6.14, please report any issue using the [issue tracker](https://github.com/wearemetabolism/shopify-theme-toolkit/issues)

## Todolist

- remove the need of {% layout 'theme' %} and {% block content %}
- add routes for account / cart / password