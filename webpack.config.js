const path = require('path');
const webpack = require("webpack");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WebpackShellPluginNext = require('webpack-shell-plugin-next');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const mode = process.env.NODE_ENV === 'development' ? 'development' : 'production';
const devtool = mode === 'development' ? 'eval-cheap-source-map' : 'nosources-source-map';
const stats = mode === 'development' ? 'errors-warnings' : { children: false };

const regexpFileLoader = /\.(woff2?|ttf|otf|eot|svg|jpe?g|png|gif|mp4|mp3|webm)$/  ;

module.exports = [
    {
        name: "dev",
        mode: "development",
        entry: ['webpack-hot-middleware/client','./theme/scripts/theme.js'],
        output: {
            path: path.resolve(__dirname, 'theme/assets'),
            filename: 'theme.js'
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ],
        resolve: {
            alias: {
                vue: 'vue/dist/vue.js'
            }
        },
        module: {
            rules: [
                {
                    test: regexpFileLoader,
                    exclude: /node_modules/,
                    loader: 'file-loader'
                },
                {
                    test: /\.scss$/,
                    use: [
                        'style-loader',
                        'css-loader',
                        'sass-loader'
                    ]
                }
            ]
        }
    },
    {
        name: "prod",
        mode: mode,
        devtool: devtool,
        entry: './theme/scripts/theme.js',
        output: {
            filename: './assets/theme.js',
            path: path.resolve(__dirname, 'dist'),
        },
        resolve: {
            alias: {
                styles: path.resolve(__dirname, 'theme/styles/'),
                vue: mode === 'production' ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js'
            }
        },
        plugins: [
            new CleanWebpackPlugin(),
            new MiniCssExtractPlugin({
                filename: './assets/theme.css',
            }),
            new CopyPlugin({
                patterns: [
                {
                    from: '**/*',
                    context: path.resolve(__dirname, "theme"),
                    globOptions: {
                        dot: true,
                        ignore: [
                            '**/.gitkeep',
                            '**/scripts',
                            '**/styles',
                            '**/assets/notifications',
                            '**/liquid'
                        ]
                    }
                },
                {
                    from: '**/*',
                    context: path.resolve(__dirname, "theme", "liquid"),
                    globOptions: {
                        dot: true,
                        ignore: [
                            '**/.gitkeep',
                            '**/notifications'
                        ]
                    }
                }
            ]})
        ],
        stats: stats,
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env'],
                    }
                },
                {
                    test: /\.(sc|sa|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                url: false
                            }
                        },
                        'postcss-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                            }
                        }
                    ]
                }
            ]
        }
    }
];


if (mode === 'development') {

    module.exports[1].plugins.push(
        new WebpackShellPluginNext({
            onBuildStart:{
                scripts: ['echo Webpack build in progress...🛠'],
            },
            onBuildEnd:{
                scripts: ['echo Build Complete 📦','theme watch','theme open'],
                parallel: true
            }
        })
    )
}