const httpError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const fs = require('fs');
const { Liquid } = require('liquidjs');
const { liquidTags } = require('./vendor/tags/index');
const { liquidFilters } = require('./vendor/filters/index');

const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const webpackHotMiddleware = require("webpack-hot-middleware");
const webpackConfig = require("./webpack.config.js");
const compiler = webpack(webpackConfig[0]);

const router = require('./vendor/router');

const globals = JSON.parse(fs.readFileSync('data/globals.json'));
globals.all_products = JSON.parse(fs.readFileSync('data/products.json'));
globals.collections = JSON.parse(fs.readFileSync('data/collections.json'));

const server = express();

const engine = new Liquid({
    root: __dirname,
    extname: '.liquid',
    globals: globals
});

engine.plugin(liquidTags())
engine.plugin(liquidFilters())

// view engine setup
server.engine('liquid', engine.express());
server.set('views', ['./theme/liquid', './theme/liquid/sections', './theme/liquid/layout', './theme/liquid/snippets']);
server.set('view engine', 'liquid') // set to default

server.use(express.static(path.join(__dirname, 'theme/assets')));

server.use(logger('dev'));

server.use(webpackDevMiddleware(compiler));
server.use(webpackHotMiddleware(compiler));

server.use('/', router);

// catch 404 and forward to error handler
server.use(function(req, res, next) {
    next(httpError(404));
});

// error handler
server.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('./templates/error');
});

module.exports = server;
